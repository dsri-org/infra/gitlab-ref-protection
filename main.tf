# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "gitlab_tag_protection" "tag_protect" {
  for_each            = local.projects
  project             = each.key
  tag                 = "*"
  create_access_level = "maintainer"
}

resource "gitlab_branch_protection" "branch_protect" {
  for_each               = local.projects
  project                = each.key
  branch                 = each.value.default_branch
  allow_force_push       = false
  push_access_level      = "maintainer"
  merge_access_level     = "maintainer"
  unprotect_access_level = "maintainer"
}

resource "random_integer" "hour" {
  for_each = local.projects
  min      = 0
  max      = 23
}

resource "random_integer" "weekday" {
  for_each = local.projects
  min      = 0
  max      = 6
}

resource "gitlab_pipeline_schedule" "weekly" {
  for_each = local.pipeline_schedules

  project     = each.key
  ref         = "refs/heads/${each.value.project.default_branch}"
  cron        = "0 ${each.value.hour} * * ${each.value.weekday}"
  description = "Terraform-managed weekly run to exercise pipelines"
}

resource "local_file" "schedule_page" {
  content = templatefile("${path.module}/templates/schedule.html.tpl", {
    schedule = local.schedule_page
  })
  filename        = "${path.module}/public/schedule/index.html"
  file_permission = "0644"
}
