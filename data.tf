# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

data "gitlab_projects" "all" {
  membership        = true
  archived          = false
  order_by          = "name"
  include_subgroups = true
  with_shared       = false
}
