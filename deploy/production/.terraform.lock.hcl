# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "16.8.1"
  constraints = "~> 16.8.1"
  hashes = [
    "h1:DckfSLS+kvajbjF98OL8qoQJW7RfluCAQXgAVHfjZYs=",
    "zh:04d2594aac4c313d8a78c97c8f389211c122beaa80d9702f2808a1429f0e41b5",
    "zh:096f61d33bb8667757ac75f3e57f21ca90b1caca9ca8beb0a75cbeb7034024ed",
    "zh:1c0af484f14e16892e4f47dad32ebb90c029583f42a1348eb23551e963d87d9f",
    "zh:2e21e59837f8d035932d22cf276dc5712ae7aeeb2cf0aadbd4abd391a8c73b3f",
    "zh:3601e8390461794ab38751762640dfe1c3e2708c8d55b4a10a1ab41e544dba34",
    "zh:3a7c27d298fabf85eec35ce1e5bf80c66866573e726772a427c28a23077ffa81",
    "zh:3be047ed882877d7ac626272a03243344fd0168df8bc806c3d42fbb297d3b809",
    "zh:65182050b36c64a7088ec28028d3871a4525c33d3f5aaf488fdeb77e081db2bd",
    "zh:9d8fe744bbae9975564c9512146b3e105631921d41b4380fc93b5a899b7ee06d",
    "zh:b35a5f0f6b46a1161f35357ae960bc4b66f6257e8d3e99331462df7c2b719fdc",
    "zh:b778d83296a7ec28659b962e61cf4dc5ea9773f5dfd81680c8ae268c20b0418b",
    "zh:c1ff7a7b0a363ba119efb1fcd7d577e5acf6807d6c799851ee9c5fd8c7510fea",
    "zh:dbc4161ad0d58fe9b45b17e023dbf07137bba401bab3d3a9a782e37861e0ad45",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
    "zh:f9b0c827d90aacdad7cb6c38b913a12469a2fe3ed20df35ade4f9d7ae84e2af2",
  ]
}

provider "registry.opentofu.org/hashicorp/local" {
  version     = "2.5.1"
  constraints = "~> 2.5.1"
  hashes = [
    "h1:GgW5qncKu4KnXLE1ZYv5iwmhSYtTNzsOvJAOQIyFR7E=",
    "zh:031c2c2070672b7e78e0aa15560839278dc57fe7cf1e58a617ac13c67b31d5fb",
    "zh:1ef64ea4f8382cd538a76f3d319f405d18130dc3280f1c16d6aaa52a188ecaa4",
    "zh:422ce45691b2f384dbd4596fdc8209d95cb43d85a82aaa0173089d38976d6e96",
    "zh:7415fbd8da72d9363ba55dd8115837714f9534f5a9a518ec42268c2da1b9ed2f",
    "zh:92aa22d071339c8ef595f18a9f9245c287266c80689f5746b26e10eaed04d542",
    "zh:9cd0d99f5d3be835d6336c19c4057af6274e193e677ecf6370e5b0de12b4aafe",
    "zh:a8c1525b389be5809a97f02aa7126e491ba518f97f57ed3095a3992f2134bb8f",
    "zh:b336fa75f72643154b07c09b3968e417a41293358a54fe03efc0db715c5451e6",
    "zh:c66529133599a419123ad2e42874afbd9aba82bd1de2b15cc68d2a1e665d4c8e",
    "zh:c7568f75ba6cb7c3660b69eaab8b0e4278533bd9a7a4c33ee6590cc7e69743ea",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.6.2"
  constraints = "~> 3.6.2"
  hashes = [
    "h1:PXvoOj9gj+Or+9k0tQWCQJKxnsVO0GqnQwVahgwRrsU=",
    "zh:1f27612f7099441526d8af59f5b4bdcc35f46915df5d243043d7337ea5a3e38a",
    "zh:2a58e66502825db8b4b96116c04bd0323bca1cf1f5752bdd8f9c26feb84d3b1e",
    "zh:4f0a4fa479e29de0c3c90146fd58799c097f7a55401cb00560dd4e9b1e6fad9d",
    "zh:9c93c0fe6ef685513734527e0c8078636b2cc07591427502a7260f4744b1af1d",
    "zh:a466ff5219beb77fb3b18a3d7e7fe30e7edd4d95c8e5c87f4f4e3fe3eeb8c2d7",
    "zh:ab33e6176d0c757ddb31e40e01a941e6918ad10f7a786c8e8e4f35e5cff81c96",
    "zh:b6eabf377a1c12cb3f9ddd97aacdd5b49c1646dc959074124f81d40fcd216d7e",
    "zh:ccec5d03d0d1c0f354be299cdd6a417b2700f1a6781df36bcce77246b2f57e50",
    "zh:d2a7945eeb691fdd2b1474da76ddc2d1655e2aedbb14b57f06d4f5123d47adf9",
    "zh:ed62351f4ad9d1469c6798b77dee5f63b18b29c473620a0046ba3d4f111b621d",
  ]
}
