# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "projects" {
  value = module.root.projects
}

output "pipeline_schedules" {
  value = module.root.pipeline_schedules
}
