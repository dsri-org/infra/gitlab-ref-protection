<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>dsri pipeline schedule</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous"><!-- pragma: allowlist secret -->
    <style>
ul, p, table {
  margin-bottom: 0 !important;
}
    </style>
  </head>
  <body>
    <header class="navbar navbar-dark bg-dark shadow-sm">
      <div class="container-fluid">
        <a href="#" class="navbar-brand d-flex align-items-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-clockwise me-2" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2z"/>
            <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466"/>
          </svg>
          <strong>dsri pipeline schedule</strong>
        </a>
      </div>
    </header>
    <table class="table table-striped table-bordered">
      <tr>
        <th></th>
        <th>Sunday</th>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thursday</th>
        <th>Friday</th>
        <th>Saturday</th>
      </tr>
      %{~ for hour in schedule ~}
      <tr>
        <th>${ hour.hour }</th>
        %{~ for weekday in hour.weekdays ~}
        <td>%{~ if length(weekday) > 0 }
          <ul>
            %{~ for project in weekday ~}
            <li><a href="${ project.url }">${ project.name }</a></li>
            %{~ endfor ~}
          </ul>
        %{ endif ~}</td>
        %{~ endfor ~}
      </tr>
      %{~ endfor ~}
    </table>
    <footer class="container py-4">
      <p class="text-center text-body-secondary">Copyright © 2024 <a href="https://ul.org">UL Research Institutes</a>. A <a href="https://dsri.org">Digital Safety Research Institute</a> project.</p>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script><!-- pragma: allowlist secret -->
  </body>
</html>
