# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  projects = {
    for project in data.gitlab_projects.all.projects : project.id => project
    if project.repository_access_level != "disabled"
  }

  pipeline_schedules = {
    for project in local.projects : project.id => {
      project = project
      hour    = random_integer.hour[project.id].result
      weekday = random_integer.weekday[project.id].result
    }
    if anytrue([
      for root in ["buildgarden", "dyff", "saferatdayzero"] : startswith(project.path_with_namespace, root)
    ])
  }

  schedule_page = [
    for hour in range(24) : {
      hour = format("%02s:00", hour)
      weekdays = [
        for weekday in range(7) : [
          for schedule in local.pipeline_schedules : {
            name = schedule.project.path_with_namespace
            url  = schedule.project.web_url
          } if schedule.hour == hour && schedule.weekday == weekday
        ]
      ]
    }
  ]
}
