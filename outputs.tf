# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

output "projects" {
  value = sort([for project in local.projects : project.path_with_namespace])
}

output "pipeline_schedules" {
  value = sort([for schedule in local.pipeline_schedules : "${schedule.project.path_with_namespace} ${schedule.weekday} ${schedule.hour}"])
}
